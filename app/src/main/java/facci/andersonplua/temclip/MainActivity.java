package facci.andersonplua.temclip;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private Button calcular;
    private EditText ingreso;
    private TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //BOTON 1
        calcular = (Button) findViewById(R.id.button1);
        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ingreso = (EditText) findViewById(R.id.editText1);
                resultado = (TextView) findViewById(R.id.textView1);
                try {
                    float gradosCen = Float.parseFloat(ingreso.getText().toString());
                    float gradosFar = 32+(gradosCen*9/5);
                    resultado.setText(String.format("R: %.2f", gradosFar));
                }catch (Exception e) {
                    resultado.setText("Ingrese solo números");
                }
            }
        });

        //BOTON 2
        calcular = (Button) findViewById(R.id.button2);
        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ingreso = (EditText) findViewById(R.id.editText2);
                resultado = (TextView) findViewById(R.id.textView2);
                try {
                    float gradosFar = Float.parseFloat(ingreso.getText().toString());
                    float gradosCent = (float) ((gradosFar-32)/ 1.8);
                    resultado.setText(String.format("R: %.2f", gradosCent));
                }catch (Exception e) {
                    resultado.setText("Ingrese solo números");
                }
            }
        });



        Log.i(TAG, "Anderson Alberto Plua Toala");
    }
}